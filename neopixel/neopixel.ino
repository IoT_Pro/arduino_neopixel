#include <Adafruit_NeoPixel.h>

int num_pixels = 4;

int led_1_pin = 11;
int led_2_pin = 9;
int led_3_pin = 7;
int neopixel_pin = 13;

int Go_btn_pin = 2;
int btn_1_pin = 3;
int btn_2_pin = 18;
int btn_3_pin = 19;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(num_pixels, neopixel_pin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel led_1_strip = Adafruit_NeoPixel(1, led_1_pin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel led_2_strip = Adafruit_NeoPixel(1, led_2_pin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel led_3_strip = Adafruit_NeoPixel(1, led_3_pin, NEO_GRB + NEO_KHZ800);

// color 1
int r1 = 255;
int g1 = 0;
int b1 = 0;

// color 2
int r2 = 255;
int g2 = 255;
int b2 = 0;

// color 3
int r3 = 0;
int g3 = 255;
int b3 = 0;

// color 4
int r4 = 0;
int g4 = 0;
int b4 = 255;

// delay time
#define DELAY_TIME 30000

// start time
unsigned long neo_start_time;
unsigned long led1_start_time;
unsigned long led2_start_time;
unsigned long led3_start_time;

// button flag
bool Go_btn_flag = false;
bool btn_1_flag = false;
bool btn_2_flag = false;
bool btn_3_flag = false;


void setup() {
  Serial.begin(9600);

  pinMode(Go_btn_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(Go_btn_pin), go_btn_handler, CHANGE);

  pinMode(btn_1_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(btn_1_pin), btn_1_handler, CHANGE);

  pinMode(btn_2_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(btn_2_pin), btn_2_handler, CHANGE);

  pinMode(btn_3_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(btn_3_pin), btn_3_handler, CHANGE);

  // strip begin
  strip.begin();
  led_1_strip.begin();
  led_2_strip.begin();
  led_3_strip.begin();
}

void loop() {
  strip.show();
  led_1_strip.show();
  led_2_strip.show();
  led_3_strip.show();
}

void go_btn_handler() {
  randomize_color();
  activate();
  delay(DELAY_TIME);
  allOff();
}

void btn_1_handler() {
  led1_strip.setPixelColor(0, 255, 0, 0);
  delay(DELAY_TIME);
  led1_strip.setPixelColor(0, 0, 0, 0);
}

void btn_2_handler() {
  led2_strip.setPixelColor(0, 0, 255, 0);
  delay(DELAY_TIME);
  led2_strip.setPixelColor(0, 0, 0, 0);
}

void btn_3_handler() {
  led3_strip.setPixelColor(0, 0, 0, 255);
  delay(DELAY_TIME);
  led3_strip.setPixelColor(0, 0, 0, 0);
}

void allOff() {
  for (int i = 0; i < num_pixels; i++) {
    strip.setPixelColor(i, 0, 0, 0);
  }
}

void activate() {
  strip.setPixelColor(0, 0, 0, 0);
  delay(random(500));
  strip.setPixelColor(0, r1, g1, b1);

  strip.setPixelColor(1, 0, 0, 0);
  delay(random(500));
  strip.setPixelColor(1, r2, g2, b2);

  strip.setPixelColor(2, 0, 0, 0);
  delay(random(500));
  strip.setPixelColor(2, r3, g3, b3);

  strip.setPixelColor(3, 0, 0 , 0);
  delay(random(500));
  strip.setPixelColor(3, r4, g4, b4);
}


void randomize_color() {
  r1 = random(255);
  g1 = random(255);
  b1 = random(255);

  r2 = random(255);
  g2 = random(255);
  b2 = random(255);

  r3 = random(255);
  g3 = random(255);
  b3 = random(255);

  r4 = random(255);
  g4 = random(255);
  b4 = random(255);
}

